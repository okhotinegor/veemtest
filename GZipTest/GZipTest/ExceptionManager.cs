﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GZipTest
{
    static class ExceptionManager
    {
        static CancellationTokenSource cancellationSource;
        static AutoResetEvent exceptionEvent;
        static bool isFail;

        static ExceptionManager()
        {
            cancellationSource = new CancellationTokenSource();
            exceptionEvent = new AutoResetEvent(false);
            isFail = false;
        }

        public static AutoResetEvent GetExceptionEvent() => exceptionEvent;
        public static bool IsFail => isFail;

        public static void Register(Exception exception)
        {
            Console.WriteLine(exception.Message);
            isFail = true;
            Cancel(null,null);
        }

        public static void Cancel(object sender, ConsoleCancelEventArgs args)
        {
            if (cancellationSource.IsCancellationRequested)
            {
                exceptionEvent.Set();
                cancellationSource.Cancel();
            }
        }

        public static CancellationToken GetCancellationToken() => cancellationSource.CreateToken();

    }
}
