﻿using System;
//using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualBasic;

namespace GZipTest
{
    static class MemoryManager
    {
        static List<ManagedThread> threads;
        static List<AutoResetEvent> outputEvents;
        static int freeBlocks;
        static bool isFinished;
        const float ramСoefficient = (1.0f / 3.0f);
        static AutoResetEvent canReadEvent;
        
        //const int otherThreadsCount = 2;//stream for write and for read

        static MemoryManager()
        {
            threads = new List<ManagedThread>(Environment.ProcessorCount);
            outputEvents = new List<AutoResetEvent>(threads.Capacity);
            freeBlocks = GetFreeRam();
            isFinished = false;
            canReadEvent = new AutoResetEvent(true);
        }

        public static bool IsFinished => isFinished;

        public static AutoResetEvent[] OutputEvents => outputEvents.ToArray();

        public static AutoResetEvent CanReadEvent => canReadEvent;

        public static bool HaveBlockForWrite()
        {
            if (!IsFinished)
            {
                return true;
            }
            else
            {
                for(int i=0; i<threads.Count; i++)
                {
                    if (threads[i].IsAlive) return true;
                }
            }
            return false;
        }

        public static void InitializeThreads(CompressionMode mode)
        {
            for(int i=0; i<threads.Capacity; i++)
            {
                threads.Add(new ManagedThread(mode, ExceptionManager.GetCancellationToken()));
                outputEvents.Add(threads[i].GetOutputEvent());

            }
        }

        public static void StartHandleThreads()
        {
            for(int i=0; i<threads.Count; i++)
            {
                threads[i].Start();
            }
        }

        public static Queue<Block> GetReadedBlocks()
        {
            if(!IsFinished)
            {
                return Reader.GetReadedBlocks();
            }
            else
            {
                if(Reader.ReadedQueuesCount>0)
                {
                    return Reader.GetReadedBlocks();
                }
                else throw new InvalidOperationException("No more blocks");
            }
        }

        public static List<Queue<Block>> GetHandledBlocks()
        {
            var result = new List<Queue<Block>>(threads.Count);
            for(int i=0; i<threads.Count; i++)
            {
                if(threads[i].BlocksForWrite>0)
                {
                    result.Add(threads[i].GetOutputBlocks());
                } 
            }
            return result;
        }

        public static void FinishRead()
        {
            isFinished = true;
            Reader.GetOutputQueueEvent().Set();
        }

        public static bool ReleaseBlockSpace()
        {
            Interlocked.Increment(ref freeBlocks);
            canReadEvent.Set();
            return true;
        }

        public static bool TakeBlockSpace()
        {
            if (freeBlocks == 0)
            {
                return false;
            }
            else
            {
                if (freeBlocks < 0)
                {
                    throw new SystemException("Memory!!!");
                }
                Interlocked.Decrement(ref freeBlocks);
                return true;
            }
        }

        private static int GetFreeRam()
        {
            PerformanceCounter сounter = new PerformanceCounter("Memory", "Available MBytes");
            var res = сounter.NextValue();
            //return Environment.ProcessorCount*5*2;
            return (int)( ((double)(res)) * ramСoefficient );
        }
    }
}
