﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Text;
using System.Threading;

namespace GZipTest
{
    static class Reader
    {
        static object lockObj;
        static Queue<Queue<Block>> queues;
        static string fileName;
        const int megabyte = 1048576;
        static Thread readThread;
        static CancellationToken token;
        //static AutoResetEvent CanReadEvent;
        static AutoResetEvent queueEvent;


        static Reader()
        {
            lockObj = new object();
            queues = new Queue<Queue<Block>>();
            queueEvent = new AutoResetEvent(false);
        }

        public static AutoResetEvent GetOutputQueueEvent()
        {
            return queueEvent;
        }

        public static void SetFileName(string name)
        {
            fileName = name;
        }

        public static void SetCancellationToken(CancellationToken token)
        {
            Reader.token = token;
        }

        public static Thread GetReadThread() => readThread;

        public static void StartRead()
        {
            var t = new Thread(Process);
            readThread = t;
            t.Priority = ThreadPriority.AboveNormal;
            t.Name = "Reader";
            t.Start();
        }

        public static int ReadedQueuesCount => queues.Count;

        public static Queue<Block> GetReadedBlocks()
        {
            Monitor.Enter(lockObj);
            try
            {
                return queues.Dequeue();
            }
            catch (InvalidOperationException e)
            {
                
                return null;
            }
            finally
            {
                Monitor.Exit(lockObj);
            }
            
        }

        private static void Process()
        {
            try
            {
                int count = 1;
                long id = 0;
                var blocks = new Queue<Block>(10);
                using (var stream = new FileStream(fileName, FileMode.Open))
                {
                    do
                    {
                        do
                        {
                            if (token.IsCancellationRequested) throw new OperationCanceledException();
                            if (MemoryManager.TakeBlockSpace())
                            {
                                var temp = new byte[1048576];
                                count = stream.Read(temp, 0, temp.Length);
                                if (count == temp.Length)
                                {
                                    blocks.Enqueue(new Block(id, temp));
                                    id++;
                                }
                                else
                                {
                                    var array = new byte[count];
                                    Array.Copy(temp, array, count);
                                    blocks.Enqueue(new Block(id, array));
                                    id++;
                                    break;
                                }
                            }
                            else
                            {
                                MemoryManager.CanReadEvent.WaitOne();
                            }
                        } while (blocks.Count != 10);
                        lock (lockObj)
                        {
                            var tmp = blocks;
                            blocks = new Queue<Block>();
                            queues.Enqueue(tmp);
                            
                            MemoryManager.CanReadEvent.Set();
                        }
                    } while (count != 0);
                    MemoryManager.FinishRead();
                    Console.WriteLine("Read ended!");
                }
            }
            catch(OperationCanceledException exception)
            {

            }
            catch(Exception e)
            {
                ExceptionManager.Cancel(null,null);
            }
        }
    }
}
