﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GZipTest
{
    class Block
    {
        private long id;

        public byte[] Data { get; set; }

        public Block(long id, byte[] data)
        {
            this.id = id;
            Data = data;
        }

        public long Id => id;
    }
}
