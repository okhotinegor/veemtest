﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace GZipTest
{
    class ManagedThread
    {
        Thread thread;
        Queue<Block> inputQueue;
        Queue<Block> outputQueue;
        MemoryStream targetStream;
        GZipStream zipStream;
        CompressionMode compressionMode;
        CancellationToken token;
        object lockObj;
        AutoResetEvent outputEvent;

        public ManagedThread(CompressionMode mode, CancellationToken token)
        {
            thread = new Thread(Process);
            inputQueue = new Queue<Block>();
            outputQueue = new Queue<Block>();
            targetStream = new MemoryStream();
            compressionMode = mode;
            zipStream = new GZipStream(targetStream, mode, true);
            lockObj = new object();
            this.token = token;
            outputEvent = new AutoResetEvent(false);
        }

        public void Start()
        {
            thread.Start();
        }

        public AutoResetEvent GetOutputEvent()
        {
            return outputEvent;
        }

        public bool IsAlive => thread.IsAlive;

        public int BlocksForWrite => outputQueue.Count;

        public Queue<Block> GetOutputBlocks()
        {
            return Interlocked.Exchange<Queue<Block>>(ref outputQueue, new Queue<Block>());
        }

        private void Process()
        {
            try
            {
                while (true)
                {
                    if (inputQueue.Count == 0)
                    {
                        var blocks = MemoryManager.GetReadedBlocks();
                        AddToQueue(blocks);
                        if (inputQueue.Count == 0)
                        {
                            if (!MemoryManager.IsFinished)
                                Reader.GetOutputQueueEvent().WaitOne();
                            else break;
                        }

                    }
                    else
                    {
                        Block block;
                        for (int i = 0; i < inputQueue.Count; i++)
                        {
                            block = inputQueue.Dequeue();
                            HandleBlock(block);
                            outputQueue.Enqueue(block);
                                outputEvent.Set();
                            
                        }
                    }
                }
            }
            catch(OperationCanceledException exception)
            {
                return;
            }
            catch(InvalidOperationException e)
            {
                
            }
            catch(Exception e)
            {
                ExceptionManager.Register(e);
            }
           
        }

        private void HandleBlock(Block block)
        {
            if (token.IsCancellationRequested) throw new OperationCanceledException();
            if (compressionMode == CompressionMode.Compress)
            {
                zipStream.Write(block.Data, 0, block.Data.Length);
                block.Data = targetStream.ToArray();
                targetStream.Position = 0;
            }
            else
            {
                targetStream.Write(block.Data, 0, block.Data.Length);
                targetStream.Position = 0;
                zipStream.ReadByte();
                int count = zipStream.Read(block.Data, 0, block.Data.Length);
                var data = new byte[count];
                Array.Copy(block.Data, data, count);
                block.Data = data;
            }
        }

        private void AddToQueue(Queue<Block> blocks)
        {
            if(blocks!=null)
                inputQueue = blocks;
        }


    }
}
