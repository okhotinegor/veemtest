﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GZipTest
{
    class CancellationTokenSource
    {
        public bool IsCancellationRequested { get; private set; }

        public void Cancel()
        {
            IsCancellationRequested = true;
        }

        public CancellationToken CreateToken()
        {
            return new CancellationToken(this);
        }
    }
}
