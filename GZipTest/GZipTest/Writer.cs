﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace GZipTest
{
    static class Writer
    {
        private static Dictionary<long, byte[]> blocks;
        private static string fileName;
        private static Thread writeThread;
        static CancellationToken token;
        static AutoResetEvent finishReadEvent;
        
        static Writer()
        {
            blocks = new Dictionary<long, byte[]>();
            finishReadEvent = new AutoResetEvent(false);
        }

        public static AutoResetEvent FinishWaitHandler()
        {
            return finishReadEvent;
        }

        public static void SetFileName(string name)
        {
            fileName = name;
        }

        public static void SetCancellationToken(CancellationToken token)
        {
            Writer.token = token;
        }

        public static Thread GetWriteThread() => writeThread;

        public static void StartWrite()
        {
            Thread t = new Thread(Process);
            t.Name = "Writer";
            t.Priority = ThreadPriority.Highest;
            writeThread = t;
            t.Start();
        }

        public static void DeleteFile()
        {
            if (token.IsCancellationRequested)
            {
                try
                {
                    File.Delete(fileName);
                }
                catch(Exception e)
                {
                    Console.WriteLine("Can't delete resuting file");
                    Console.WriteLine(e.Message);
                }
            }
            else throw new InvalidOperationException();
        }

        private static void AddToDictionary(List<Queue<Block>> queues)
        {
            for (int i = 0; i < queues.Count; i++)
            {
                foreach(var tmp in queues[i])
                {
                    blocks.Add(tmp.Id, tmp.Data);
                }
            }
        }

        private static void Process()
        {
            try
            {
                int id = 0;
                byte[] data;
                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    while (MemoryManager.HaveBlockForWrite())
                    {
                        if (token.IsCancellationRequested) throw new OperationCanceledException();
                        var temp = MemoryManager.GetHandledBlocks();
                        if (temp.Count == 0)
                        {
                            if (!MemoryManager.IsFinished)
                                AutoResetEvent.WaitAny(MemoryManager.OutputEvents);
                            else break;
                        }
                        else AddToDictionary(temp);

                        while (blocks.Count != 0)
                        {
                            if (blocks.ContainsKey(id))
                            {
                                data = blocks[id];
                                blocks.Remove(id);
                                id++;
                                stream.Write(data, 0, data.Length);
                                data = null;
                                MemoryManager.ReleaseBlockSpace();
                                //MemoryManager.CanReadEvent.Set();

                            }
                            else break;
                        }
                    }

                    if (token.IsCancellationRequested) throw new OperationCanceledException();
                    var buffer = MemoryManager.GetHandledBlocks();
                    if (buffer.Count == 0) goto end;
                    else AddToDictionary(buffer);

                    while (blocks.Count != 0)
                    {
                        data = blocks[id];
                        blocks.Remove(id);
                        id++;
                        stream.Write(data, 0, data.Length);
                        data = null;
                        MemoryManager.ReleaseBlockSpace();
                        //MemoryManager.CanReadEvent().Set();
                    }
                }
                end:
                finishReadEvent.Set();
            }
            catch(OperationCanceledException exception)
            {

            }
            catch(Exception e)
            {
                ExceptionManager.Register(e);
            }
        }
    }
}
