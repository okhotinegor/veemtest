﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO.Compression;
using System.Threading;
using System.Diagnostics;

namespace GZipTest
{
    class Program
    {
        static Program()
        {
        }
        
        static void Main(string[] args)
        {
            try
            {
                if (IsValidCommand(args[0]))
                {
                    CompressionMode mode;
                    if (args[0].Equals("c")) mode = CompressionMode.Compress;
                    else mode = CompressionMode.Decompress;
                    if (IsExistFile(args[1]))
                    {
                        if (IsValidFileName(args[2]))
                        {
                            Reader.SetFileName(args[1]);
                            Reader.SetCancellationToken(ExceptionManager.GetCancellationToken());
                            Writer.SetFileName(args[2] + ".gzip");
                            Writer.SetCancellationToken(ExceptionManager.GetCancellationToken());
                            Console.CancelKeyPress += new ConsoleCancelEventHandler(ExceptionManager.Cancel);
                            MemoryManager.InitializeThreads(mode);
                            Reader.StartRead();
                            MemoryManager.StartHandleThreads();
                            Writer.StartWrite();
                        }
                        else
                        {
                            Console.WriteLine("Destination file name is not correct!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Source file name is not correct!");
                    }
                }
                else
                {
                    Console.WriteLine("Unknown command!");
                    PrintHelp();
                }
            }
            catch(OutOfMemoryException exception)
            {
            }
            catch(StackOverflowException exception)
            {
            }
            AutoResetEvent.WaitAny(new[] { Writer.FinishWaitHandler(), ExceptionManager.GetExceptionEvent() });
            if (ExceptionManager.GetCancellationToken().IsCancellationRequested)
            {
                Console.WriteLine("Operation Cancelled");
            }
            else if(ExceptionManager.IsFail)
            {
                Console.WriteLine("Program failed");
                Writer.DeleteFile();
            }
            else Console.WriteLine("Finished");

            Console.ReadKey();
        }

        static bool IsValidFileName(string name)
        {
            var res = RemoveInvalidChars(name);
            if (res.Length != name.Length) return false;
            else return true;
        }

        public static string RemoveInvalidChars(string fileName)
        {
            foreach (var ch in Path.GetInvalidFileNameChars())
            {
                fileName = fileName.Replace(oldValue: ch.ToString(), newValue: "");
            }
            return fileName;
        }

        static bool IsValidCommand(string commandName)
        {
            return (commandName.Equals("c") | commandName.Equals("d"));
        }

        static void PrintHelp()
        {
            Console.WriteLine("All valid commands:\n" +
                "c [source file name] [compressed file name]\n" +
                "d [source file name] [decompressed file name]"
                );
        }

        static bool IsExistFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                if (fileName.Contains(".gzip"))
                {
                    Console.WriteLine("File already compressed");
                    return false;
                }
                else return true;
            }
            else return false;
            
        }
    }
}
