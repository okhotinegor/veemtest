﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GZipTest
{
    struct CancellationToken
    {
        public CancellationToken(CancellationTokenSource source)
        {
            Source = source;
        }
        public CancellationTokenSource Source { get; }
        public bool IsCancellationRequested { get { return Source.IsCancellationRequested; } }
        
    }
}
